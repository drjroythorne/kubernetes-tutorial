# Rough instructions for setting up a local MariaDB container and running migrations on it using a K8S job 


## Start empty MariaDB container.

    docker run --rm -p3333:3306 --name phpapp-db -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=phpapp -e MYSQL_USER=admin -e MYSQL_PASSWORD=password -d mariadb

## Create first migration with Phinx

This is used to create files like those in db/migrations

    docker run --rm -v "$PWD:/app" vegbrasil/phinx create initialize

Then execute the migrations with 

    docker run --rm -v "$PWD:/app" vegbrasil/phinx migrate

## Minikube

kvm2 driver uses 192.168.122.1 to access host from within minikube e.g. if I run python -m http.server on the host, then I can access it via:

    minikube ssh

    $ curl 192.168.122.1:8000
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Directory listing for /</title>
    </head>
    <body>
    <h1>Directory listing for /</h1>
    <hr>
    <ul>
    </ul>
    <hr>
    </body>
    </html>


## Access db from host to check migrations have run

    mysql --host=127.0.0.1 --user=admin --port=3333 --password phpapp
    Enter password: 
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A

    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 11
    Server version: 5.5.5-10.4.6-MariaDB-1:10.4.6+maria~bionic mariadb.org binary distribution

    Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    mysql> show tables;
    +------------------+
    | Tables_in_phpapp |
    +------------------+
    | phinxlog         |
    | user             |
    +------------------+
    2 rows in set (0.00 sec)

## To add to README

Construct the migrations container

    docker build --tag trdroythorne/phpapp-migrations:v3.0.0 .

(Tie the version number to the application number for now.)

Then login and push to dockerhub.

