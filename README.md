Access running container minikube

    kubectl exec -it php-fb5d46978-nbhqc -- /bin/bash

### Patch the annotation to force a redeployment when there is no version number change

     kubectl patch deployment nginx -p   "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"date\":\"`date +'%s'`\"}}}}}"

(In production, you'd never do this when changing a configmap, instead create another configmap version then update the deployment to point to the new configuration. That way you have the history and can roll back.)


## Additional notes on DO K8S

Setup cluster as per https://www.digitalocean.com/docs/kubernetes/quickstart/

In this case, we've named it intelligence-test-1-k8s-1-13-5-lon1, running k8s 1.13.5 in region London 1 (lon-1).

## Install kubectl

Assuming you're running Ubuntu, install kubectl on the controller machine (https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux)

    sudo apt-get update && sudo apt-get install -y apt-transport-https
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
    sudo apt-get update
    sudo apt-get install -y kubectl

Also, install the autocompletion (it's got lots of CLI options!)

However, the completion script depends on bash-completion, which means that you have to install this software first (you can test if you have bash-completion already installed by running type _init_completion).

### Install bash-completion

bash-completion is provided by many package managers (see here). You can install it with apt-get install bash-completion or yum install bash-completion, etc.

The above commands create /usr/share/bash-completion/bash_completion, which is the main script of bash-completion. Depending on your package manager, you have to manually source this file in your ~/.bashrc file.

To find out, reload your shell and run type _init_completion. If the command succeeds, you’re already set, otherwise add the following to your ~/.bashrc file:

    source /usr/share/bash-completion/bash_completion

Reload your shell and verify that bash-completion is correctly installed by typing type _init_completion.

### Enable kubectl autocompletion

https://kubernetes.io/docs/tasks/tools/install-kubectl/#optional-kubectl-configurations

You now need to ensure that the kubectl completion script gets sourced in all your shell sessions. There are two ways in which you can do this:

Source the completion script in your ~/.bashrc file:

    echo 'source <(kubectl completion bash)' >>~/.bashrc

## Local K8S dev

For playing with this locally, I'm using minikube on KVM

## Install KVM

https://www.linuxtechi.com/install-configure-kvm-ubuntu-18-04-server/

    sudo apt install qemu qemu-kvm libvirt-bin  bridge-utils  virt-manager ubuntu-vm-builder

	sudo update-rc.d libvirtd enable

	sudo addgroup libvirtd

	sudo adduser `id -un` libvirtd


Logout and log back in. You can test if your install has been successful with the following command:

    $ virsh list --all

     Id Name                 State

    ----------------------------------

## Install minikube

	curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \

  && chmod +x minikube

	sudo cp minikube /usr/local/bin && rm minikube

### Install KVM driver

https://github.com/kubernetes/minikube/blob/master/docs/drivers.md#kvm-driver

    curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2 \
    sudo install docker-machine-driver-kvm2 /usr/local/bin/
    sudo systemctl enable libvirtd.service
    sudo systemctl start libvirtd.service
    sudo systemctl status libvirtd.service


    newgrp libvirt 

### Start minikube

Remember to unset KUBECONFIG if it's been configured to point to other clusters. (https://github.com/kubernetes/minikube/issues/4100)

    unset KUBECONFIG

Then start minikube (image-pull-progress-deadline set to 30 minutes for my bad 4G on way to London Paddington.):

    minikube start --vm-driver kvm2 --cpus 4 --memory 8192 --extra-config=kubelet.image-pull-progress-deadline=30m    
Minikube is using 1.14.0 whereas DO is on 1.13.5 (2019-05-02).

Check the system startup progress with:

    watch kubectl get pods -n kube-system

Check the virtual machine details with virsh

    sudo virsh list --all
     Id    Name                           State
    ----------------------------------------------------
     5     minikube                       running

    ✔ ~/dev/thoughtriver/kubernetes-tutorial [master|✔] 
    11:46 $ sudo virsh net-list --all
     Name                 State      Autostart     Persistent
    ----------------------------------------------------------
     default              active     yes           yes
     minikube-net         active     yes           yes

### Install helm

https://github.com/helm/helm/blob/master/docs/install.md

## Ingress

	minikube addons enable ingress

Check its working

	kubectl get pods -n kube-system	

## DockerHub

Image definitions now tagged and pushed to DockerHub as public images (no confidential stuff please!). 

Login to DockerHub

    docker login --username=trdroythorne --password={PASSWORD}

Tag and push the images
    
    pushd src/docker/nginx
    docker build --tag trdroythorne/nginx-phpapp/v1.0.0 .
    docker push trdroythorne/nginx-phpapp/v1.0.0
    popd
    pushd src/docker/php-fpm
    docker build --tag trdroythorne/phpapp/v1.0.0 .
    docker push trdroythorne/phpapp/v1.0.0
    popd

## Private registry (untested)

- (Insecure registry)[https://github.com/kubernetes/minikube/blob/master/docs/insecure_registry.md]
- (K8S docs for private registries) https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/]

## Deploy everything

Run `kubectl apply -f {YML_FILE}` for the following files (in this order):

      storage.yml - configure some storage on the minikube host
      claim.yml - portion off some of this to bind to our deployments
      ingress.yml - portal to the outside world (using host k8s-php.com)
      nginx-configmap.yml - NGINX configuration
      secret.yml - Example of including a secret.
      phpapp-db-secret.yml - External database password.
      nginx-service.yml
      php-service.yml
      nginx-deployment.yml
      phpapp-db-service.yml
      phpapp-db-endpoint.yml
      phpapp-migrations-job.yml
      phpapp-seed-job.yml
      php-deployment.yml

Test this by adding the `minikube ip` to k8s-php.com in /etc/hosts. You should get our custom index.html by navigating to k8s-php.com, and k8s-php.com/index.php (or, indeed, anything ending in .php) will kive the php config page being served from the php container.

## Terraform, DigitalOcean and Kuberntes

	terraform apply -var='do_token=<<API_TOKEN>>'
	terraform output client_certificate > kube_config.yml
	export KUBECONFIG=$KUBECONFIG:$PWD/kube_config.yml
	kubectl config view
	kubectl config use-context do-lon1-tutorial


## K8S from scratch on bare metal -  GCloud with kubeadm.

We could easily try the same thing on DigitalOcean - need to know how to administer this where we can't use a managed K8S service.

Create k8s master node VM:

    gcloud beta compute --project=brave-pipe-188114 instances create k8s-master --description=Kubernetes\ master --zone=europe-west2-c --machine-type=custom-2-4096 --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=752296828246-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --tags=http-server,https-server --image=ubuntu-1804-bionic-v20181003 --image-project=ubuntu-os-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=k8s-master --labels=kubernetes_cluster=test1,kubernetes_master=true

    gcloud compute --project=brave-pipe-188114 firewall-rules create default-allow-http --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:80 --source-ranges=0.0.0.0/0 --target-tags=http-server

    gcloud compute --project=brave-pipe-188114 firewall-rules create default-allow-https --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:443 --source-ranges=0.0.0.0/0 --target-tags=https-server



### GCloud install

	export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
	echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
	curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
	sudo apt-get update && sudo apt-get install -y google-cloud-sdk

### ssh to =master after setting up glcoud on 'control' machine

    gcloud init
    gcloud compute ssh "k8s-master"

On master, update, install kubeadm and install networking layer

	sudo apt update && sudo apt upgrade
	sudo apt-get install -y apt-transport-https curl
	sudo su -
	curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
	cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
    deb https://apt.kubernetes.io/ kubernetes-xenial main
    EOF	
	apt-get update
	apt-get install -y kubelet kubeadm kubectl
	apt-mark hold kubelet kubeadm kubectl

Docker install (https://kubernetes.io/docs/setup/cri/)

	sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -		sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
	sudo apt install -y docker-ce=18.06.1~ce~3-0~ubuntu
	sudo su -
	cat > /etc/docker/daemon.json <<EOF
    {
      "exec-opts": ["native.cgroupdriver=systemd"],
      "log-driver": "json-file",
      "log-opts": {
        "max-size": "100m"
      },
      "storage-driver": "overlay2"
    }
    EOF
	exit
	sudo mkdir -p /etc/systemd/system/docker.service.d
	sudo systemctl daemon-reload
	sudo systemctl restart docker

### Install the networking layer

Now create a cluster (where 35.242.160.79 is the external IP address of the master)

	sudo kubeadm init --pod-network-cidr=192.168.0.0/16 --apiserver-cert-extra-sans=35.242.160.79
	mkdir -p $HOME/.kube
	sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
	sudo chown $(id -u):$(id -g) $HOME/.kube/config

Install Calcio and the right RBAC roles (https://docs.projectcalico.org/v3.2/getting-started/kubernetes/)

	kubectl apply -f \
    https://docs.projectcalico.org/v3.2/getting-started/kubernetes/installation/hosted/etcd.yaml

	kubectl apply -f \
    https://docs.projectcalico.org/v3.2/getting-started/kubernetes/installation/rbac.yaml

	kubectl apply -f \
    https://docs.projectcalico.org/v3.2/getting-started/kubernetes/installation/hosted/calico.yaml

You should see something like the following when running

    watch kubectl get pods --all-namespaces

    Every 2.0s: kubectl get pods --all-namespaces                                                                                                                                  k8s-master: Thu Oct 18 19:55:26 2018

    NAMESPACE     NAME                                      READY   STATUS    RESTARTS   AGE
    kube-system   calico-etcd-skc2s                         1/1     Running   0          41s
    kube-system   calico-kube-controllers-f4dcbf48b-745qw   1/1     Running   0          58s
    kube-system   calico-node-wr67g                         2/2     Running   2          57s
    kube-system   coredns-576cbf47c7-2dsz7                  1/1     Running   0          3m34s
    kube-system   coredns-576cbf47c7-c8qbc                  1/1     Running   0          3m34s
    kube-system   etcd-k8s-master                           1/1     Running   0          3m   
    kube-system   kube-apiserver-k8s-master                 1/1     Running   0          2m39s
    kube-system   kube-controller-manager-k8s-master        1/1     Running   0          2m42s
    kube-system   kube-proxy-bzv8p                          1/1     Running   0          3m34s
    kube-system   kube-scheduler-k8s-master                 1/1     Running   0          2m33s

Allow to runpods on master:

	kubectl taint nodes --all node-role.kubernetes.io/master-

And confirm we have a node on master:

	dan@k8s-master:~$ kubectl get nodes -o wide

    NAME         STATUS   ROLES    AGE     VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION    CONTAINER-RUNTIME

    k8s-master   Ready    master   5m45s   v1.12.1   10.154.0.2    <none>        Ubuntu 18.04.1 LTS   4.15.0-1021-gcp   docker://18.6.1

### Install kubeadm on the workers and run a job

	kubeadm join 10.154.0.2:6443 --token vpa6dy.h18tb6y5zlpthlow --discovery-token-ca-cert-hash sha256:4ab3c22ad4d4899d9ce6c1ab86e3b898e70dcf1f67e8b7e1f402d9cff300bc1b

(The   [WARNING RequiredIPVSKernelModulesAvailable]: is not an issue (https://github.com/kubernetes/kubeadm/issues/975))

### Make my cluster administrable locally

Copy admin to local file.

Locally:

	gcloud compute scp dan@k8s-master:~/.kube/config ./admin.conf

Change the IP address in admin.conf to the masters external IP

Now check we have a K8S cluster up and running:

	 kubectl --kubeconfig ./admin.conf get nodes

Move admin to ~/kube.config where it's picked up by default.

### Install the kubernetes dashboard:

#### Heapster

	kubectl apply --filename https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
	kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml

To get dashboard working:

 - Replace the grafana deployment in Heapster with NodePort
 - Run influxdb, grafana, then yaml in heapster deployment
 - Heapster RBAC yaml
 - Deploy dashboard yaml
 - Add full permissions RBAC k8stest/dashboard-rbac
 - Heapster isn't actually getting the metrics from pods - must look in to this.

### Deploying the Dockerfile

For a public container on docker hub (push the ccldroythorne/hello app to Docker hub first), simply run (maybe need to modify ImagePullPolicy)

    kubectl run hello-node --image=ccldroythorne/hello --port=8080

## References

### Presentation

http://kube-decon.carson-anderson.com/ (based on a YouTube talk)

