<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'name'    => 'Donald'
            ],[
                'user_id' => 2,
                'created' => date('Y-m-d H:i:s'),
                'name'    => 'Bob'
            ]
        ];

        $posts = $this->table('user');
        $posts->insert($data)
              ->save();
    }
}
