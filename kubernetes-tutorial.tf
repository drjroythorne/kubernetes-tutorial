variable "do_token" {}

output "client_certificate" {
  value = "${digitalocean_kubernetes_cluster.tutorial.kube_config.0.raw_config}"
}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_database_cluster" "tutorial" {
  name = "tutorialdb"
  engine = "pg"
  version = "11"
  size = "db-s-1vcpu-1gb"
  region = "lon1"
  node_count = 1
}

resource "digitalocean_kubernetes_cluster" "tutorial" {
  name    = "tutorial"
  region  = "lon1"
  version = "1.14.2-do.0"
  tags    = ["tutorial", "intelligence"]

  node_pool {
    name       = "tutorial-pool"
    size       = "s-2vcpu-2gb"
    node_count = 1
  }
}

provider "kubernetes" {
  host = "${digitalocean_kubernetes_cluster.tutorial.endpoint}"

  client_certificate     = "${base64decode(digitalocean_kubernetes_cluster.tutorial.kube_config.0.client_certificate)}"
  client_key             = "${base64decode(digitalocean_kubernetes_cluster.tutorial.kube_config.0.client_key)}"
  cluster_ca_certificate = "${base64decode(digitalocean_kubernetes_cluster.tutorial.kube_config.0.cluster_ca_certificate)}"
}

resource "digitalocean_project" "tutorial" {
    name        = "Intelligence Research"
    description = "Machine learning / AI / Data processing / R&D for the Intelligence Team"
    purpose     = "Development"
    environment = "Development"
    resources   = ["do:kubernetes:${digitalocean_kubernetes_cluster.tutorial.id}"]
}

resource "kubernetes_secret" "tutorial" {
  metadata {
    name = "db-secure"
  }

  data {
    db_admin_username = "${base64encode(digitalocean_database_cluster.tutorial.user)}"
    db_admin_password = "${base64encode(digitalocean_database_cluster.tutorial.password)}"
  }
}

resource "kubernetes_config_map" "tutorial" {
  metadata {
    name = "db-parameters"
  }

  data {
    db_host = "${digitalocean_database_cluster.tutorial.host}"
    db_port = "${digitalocean_database_cluster.tutorial.port}"
    db_database = "${digitalocean_database_cluster.tutorial.database}"
  }
}
