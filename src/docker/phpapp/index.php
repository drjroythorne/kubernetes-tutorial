<html>
 <head>
  <title>PHP Hello</title>
 </head>
 <body>
 <?php 
  require_once 'vendor/autoload.php';
  $secret = getenv('PHPSECRET', 'DEFAULTSECRET'); 
  echo '<p>Hello from PHP with secret ' . $secret . '</p>';

  $username = 'admin';
  $password = getenv('PHPAPP_DB_PASSWORD');
  $dsn = 'mysql:host=phpapp-db;dbname=phpapp;port=3306;charset=utf8';
  $pdo = new \FaaPz\PDO\Database($dsn, $username, $password);

  try {
    foreach($pdo->query('SELECT * from user') as $row) {
      echo $row['name']. "\n";
    }
  } catch (\FaaPz\PDO\Exception $e) {
    print "Error: " . $e->getMessage() . "<br/>";
    die();
  }
 ?> 
 </body>
</html>

